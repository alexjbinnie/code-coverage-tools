// Copyright (c) 2019 Alex Jamieson-Binnie. All rights reserved.
// Licensed under the MIT License. See LICENSE.txt in the project root for license information.

namespace CodeCoverageTools
{
    /// <summary>
    /// Base class for defining the coverage of a method or collection of methods.
    /// </summary>
    public abstract class BaseCoverage
    {
        /// <summary>
        /// Display name of the statistic.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Number of covered statements.
        /// </summary>
        public abstract int CoveredStatements { get; }

        /// <summary>
        /// Total number of statements covered.
        /// </summary>
        public abstract int TotalStatements { get; }

        /// <summary>
        /// Fraction of statements which are covered.
        /// </summary>
        public float StatementCoverage => 1f * CoveredStatements / TotalStatements;

        /// <summary>
        /// Percentage statements which are covered.
        /// </summary>
        public int PercentageCoverage => (int) (100 * StatementCoverage);
    }
}
