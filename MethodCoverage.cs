// Copyright (c) 2019 Alex Jamieson-Binnie. All rights reserved.
// Licensed under the MIT License. See LICENSE.txt in the project root for license information.

namespace CodeCoverageTools
{
    /// <summary>
    /// Coverage statistic for a single method.
    /// </summary>
    public class MethodCoverage : BaseCoverage
    {
        /// <summary>
        /// Create a <see cref="MethodCoverage"/> given the total number of statements and the number of which have been triggered.
        /// </summary>
        public MethodCoverage(int totalStatements, int coveredStatements)
        {
            CoveredStatements = coveredStatements;
            TotalStatements = totalStatements;
        }

        /// <inheritdoc cref="BaseCoverage.CoveredStatements"/>
        public override int CoveredStatements { get; }

        /// <inheritdoc cref="BaseCoverage.TotalStatements"/>
        public override int TotalStatements { get; }
    }
}
