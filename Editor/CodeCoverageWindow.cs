﻿// Copyright (c) 2019 Alex Jamieson-Binnie. All rights reserved.
// Licensed under the MIT License. See LICENSE.txt in the project root for license information.

using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;
using UnityEngine.TestTools;

namespace CodeCoverageTools.Editor
{
    /// <summary>
    /// Editor window that shows code coverage statistics.
    /// </summary>
    public class CodeCoverageWindow : EditorWindow
    {
        [MenuItem("Window/Analysis/Code Coverage")]
        public static void ShowWindow()
        {
            GetWindow(typeof(CodeCoverageWindow));
        }

        private void Awake()
        {
            titleContent = new GUIContent("Code Coverage");
        }

        /// <summary>
        /// Root coverage collection for the entire project.
        /// </summary>
        private CoverageCollection projectCoverage = new CoverageCollection();

        private CodeCoverageTreeView treeView;
        private TreeViewState treeViewState;
        private MultiColumnHeaderState headerState;


        void OnEnable()
        {
            if (Coverage.enabled)
            {
                if (treeViewState == null)
                    treeViewState = new TreeViewState();
                if (headerState == null)
                {
                    headerState = CodeCoverageTreeView.CreateHeaderState();
                }

                RefreshAssemblyCoverage();

                var column = new MultiColumnHeader(headerState);

                treeView = new CodeCoverageTreeView(treeViewState, column, projectCoverage);
            }
        }

        private void RefreshAssemblyCoverage()
        {
            projectCoverage = CodeCoverageUtility.GetCoverage(AssemblyFilter);
            if (treeView != null)
                treeView.Coverage = projectCoverage;
        }

        [MenuItem("Tools/Clear Coverage Stats")]
        public static void ClearStats()
        {
            if (Coverage.enabled)
                Coverage.ResetAll();
        }

        void OnGUI()
        {
            if (!Coverage.enabled)
            {
                EditorGUILayout.HelpBox(
                    "Enable code coverage in Preferences/General to use this tool",
                    MessageType.Error);
                return;
            }

            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar, GUILayout.ExpandWidth(true));

            if (GUILayout.Button("Reset Coverage",
                                 EditorStyles.toolbarButton,
                                 GUILayout.MaxWidth(160f)))
            {
                ClearStats();
                RefreshAssemblyCoverage();
                treeView.Reload();
            }

            if (GUILayout.Button("Calculate Coverage",
                                 EditorStyles.toolbarButton,
                                 GUILayout.MaxWidth(160f)))
            {
                RefreshAssemblyCoverage();
                treeView.Reload();
            }

            AssemblyFilter = GUILayout.TextField(AssemblyFilter,
                                                 EditorStyles.toolbarTextField,
                                                 GUILayout.ExpandWidth(true));

            EditorGUILayout.EndHorizontal();

            var toolbarHeight = 16f;
            treeView.OnGUI(new Rect(new Vector2(0, toolbarHeight),
                                    position.size - new Vector2(0, toolbarHeight)));
        }

        private const string AssemblyFilterKey = "codecoveragewindow.assemblyfilter";

        private string AssemblyFilter
        {
            get => EditorPrefs.GetString(AssemblyFilterKey, "");
            set => EditorPrefs.SetString(AssemblyFilterKey, value);
        }
    }
}
