// Copyright (c) 2019 Alex Jamieson-Binnie. All rights reserved.
// Licensed under the MIT License. See LICENSE.txt in the project root for license information.

using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace CodeCoverageTools.Editor
{
    /// <summary>
    /// A <see cref="TreeView"/> for visualising a <see cref="CoverageCollection"/>
    /// </summary>
    internal class CodeCoverageTreeView : TreeView
    {
        /// <summary>
        /// Columns to display in the code coverage table.
        /// </summary>
        enum Columns
        {
            Name,
            Coverage,
            Statements
        }

        public CodeCoverageTreeView(TreeViewState state,
                                    MultiColumnHeader header,
                                    BaseCoverage coverage) : base(state, header)
        {
            Coverage = coverage;

            showBorder = true;
            showAlternatingRowBackgrounds = true;

            Reload();
        }

        public BaseCoverage Coverage { get; set; }

        protected override void RowGUI(RowGUIArgs args)
        {
            var item = (CodeCoverageTreeViewItem) args.item;

            for (int i = 0; i < args.GetNumVisibleColumns(); ++i)
            {
                CellGUI(args.GetCellRect(i), item, (Columns) args.GetColumn(i),
                        ref args);
            }
        }

        void CellGUI(Rect cellRect,
                     CodeCoverageTreeViewItem item,
                     Columns column,
                     ref RowGUIArgs args)
        {
            CenterRectUsingSingleLineHeight(ref cellRect);

            switch (column)
            {
                case Columns.Name:
                    args.rowRect = cellRect;
                    base.RowGUI(args);
                    break;
                case Columns.Coverage:
                    EditorGUI.ProgressBar(cellRect,
                                          item.Stats.StatementCoverage + 0.01f,
                                          $"{item.Stats.PercentageCoverage}%");
                    break;
                case Columns.Statements:
                    GUI.Label(
                        cellRect,
                        $"{item.Stats.CoveredStatements}/{item.Stats.TotalStatements}");
                    break;
            }
        }

        public CodeCoverageTreeViewItem GetTreeViewItem(BaseCoverage coverage)
        {
            var item = new CodeCoverageTreeViewItem
            {
                displayName = coverage.Name,
                Stats = coverage
            };
            if (coverage is CoverageCollection collection)
                foreach (var child in collection.Children)
                    item.AddChild(GetTreeViewItem(child));

            return item;
        }

        protected override TreeViewItem BuildRoot()
        {
            var root = GetTreeViewItem(Coverage);

            root.depth = -1;

            var i = 0;
            SetupIds(root, ref i);

            SetupDepthsFromParentsAndChildren(root);

            return root;
        }

        private void SetupIds(TreeViewItem root, ref int i)
        {
            if (root == null)
                return;
            root.id = i++;
            if (root.hasChildren)
                foreach (var children in root.children)
                    SetupIds(children, ref i);
        }

        public static MultiColumnHeaderState CreateHeaderState()
        {
            return new MultiColumnHeaderState(new[]
            {
                new MultiColumnHeaderState.Column
                {
                    headerContent = new GUIContent("Name"),
                    width = 384f,
                    canSort = false
                },
                new MultiColumnHeaderState.Column
                {
                    headerContent = new GUIContent("Coverage"),
                    canSort = false
                },
                new MultiColumnHeaderState.Column
                {
                    headerContent = new GUIContent("Statements"),
                    canSort = false
                }
            })
            {
            };
        }
    }
}