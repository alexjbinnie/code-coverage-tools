// Copyright (c) 2019 Alex Jamieson-Binnie. All rights reserved.
// Licensed under the MIT License. See LICENSE.txt in the project root for license information.

using UnityEditor.IMGUI.Controls;

namespace CodeCoverageTools.Editor
{
    /// <summary>
    /// Extension of a <see cref="TreeViewItem"/> which contains a reference to the <see cref="BaseCoverage"/> it represents.
    /// </summary>
    internal class CodeCoverageTreeViewItem : TreeViewItem
    {
        public BaseCoverage Stats { get; set; }
    }
}
