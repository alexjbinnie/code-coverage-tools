// Copyright (c) 2019 Alex Jamieson-Binnie. All rights reserved.
// Licensed under the MIT License. See LICENSE.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine.TestTools;

namespace CodeCoverageTools
{
    public class CodeCoverageUtility {

        public static CoverageCollection GetCoverage(string assemblyFilter)
        {
            var collection = new CoverageCollection();

            if (!Coverage.enabled)
                return null;

            IEnumerable<Assembly> assemblies = AppDomain.CurrentDomain
                                                        .GetAssemblies();

            if (!string.IsNullOrEmpty(assemblyFilter))
            {
                var regex = new Regex(assemblyFilter);
                assemblies = assemblies.Where(assembly => regex.IsMatch(assembly.GetName().Name));
            }

            foreach (var assembly in assemblies)
            {
                var types = assembly.GetTypes();
                foreach (var type in types)
                {
                    var methods = type.GetMethods(
                        BindingFlags.Public
                      | BindingFlags.NonPublic
                      | BindingFlags.Instance
                      | BindingFlags.Static
                      | BindingFlags.DeclaredOnly);

                    var methodCoverages = new List<MethodCoverage>();
                    foreach (var method in methods)
                    {
                        var stat = Coverage.GetStatsFor(method);
                        var total = stat.totalSequencePoints;
                        var covered = total - stat.uncoveredSequencePoints;

                        if (total > 0)
                        {
                            var methodCoverage = new MethodCoverage(total, covered)
                            {
                                Name = method.Name
                            };
                            methodCoverages.Add(methodCoverage);
                        }
                    }

                    if (methodCoverages.Count > 0)
                    {
                        var coverageCollection = GetCoverageCollection(collection, type);
                        foreach (var methodCoverage in methodCoverages)
                            coverageCollection.Children.Add(methodCoverage);
                    }
                }
            }

            return collection;
        }

        private static CoverageCollection GetCoverageCollection(CoverageCollection rootCollection, Type type)
        {
            var namespaces = new List<string>();
            namespaces.AddRange(type.Namespace?.Split('.') ?? new string[0]);
            if (type.DeclaringType != null)
                namespaces.Add(type.DeclaringType.Name);

            namespaces.Add(type.Name);
            var collection = rootCollection;
            CoverageCollection existing;
            foreach (var @namespace in namespaces)
            {
                existing = collection.Children
                                     .FirstOrDefault(child => child.Name.Equals(@namespace))
                               as CoverageCollection;

                if (existing == null)
                {
                    existing = new CoverageCollection()
                    {
                        Name = @namespace
                    };
                    collection.Children.Add(existing);
                }

                collection = existing;
            }

            return collection;
        }

    }
}
