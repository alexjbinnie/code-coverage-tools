// Copyright (c) 2019 Alex Jamieson-Binnie. All rights reserved.
// Licensed under the MIT License. See LICENSE.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using CodeCoverageTools;

namespace CodeCoverageTools
{
    /// <summary>
    /// Statistics for a collection of other coverage statistics.
    /// </summary>
    public class CoverageCollection : BaseCoverage
    {
        /// <inheritdoc cref="BaseCoverage.TotalStatements"/>
        public override int TotalStatements => Children.Sum(c => c.TotalStatements);

        /// <inheritdoc cref="BaseCoverage.CoveredStatements"/>
        public override int CoveredStatements => Children.Sum(c => c.CoveredStatements);

        /// <summary>
        /// Set of child coverage statistics.
        /// </summary>
        public List<BaseCoverage> Children { get; } = new List<BaseCoverage>();
    }
}
